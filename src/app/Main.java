package app;

import business.DeliveryService;
import presentation.LoginFrame;
import presentation.ProductTableFrame;

import javax.swing.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class Main extends DeliveryService {
    public static DeliveryService deliveryService;
    public static JFrame GUI;
    public static ProductTableFrame productTableFrame;

    public static void main(String[] args){
        // deserialize
        try {
            FileInputStream f = new FileInputStream("DeliveryService.txt");
            ObjectInputStream o = new ObjectInputStream(f);
            deliveryService = (DeliveryService) o.readObject();
            f.close();
            o.close();
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("Nu s-a putut realiza deserializarea!");
            deliveryService = new DeliveryService();
            deliveryService.serialize();
        }
        GUI = new LoginFrame();
    }
}
