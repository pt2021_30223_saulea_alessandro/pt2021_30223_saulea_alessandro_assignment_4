package business;

import presentation.EmployeeFrame;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import static app.Main.productTableFrame;

public class DeliveryService extends Observable implements IDeliveryServiceProcessing, Serializable {

    private final Map<Order,ArrayList<MenuItem>> orderMap;
    private final ArrayList<MenuItem> menuItems;
    private final ArrayList<Order> orders;
    private final ArrayList<User> users;
    private int nextProductID;
    private int nextOrderID;

    public DeliveryService() {
        orderMap = new HashMap<>();
        menuItems = new ArrayList<>();
        orders = new ArrayList<>();
        users = new ArrayList<>();
        nextProductID = 0;
        nextOrderID = 0;
    }

    public Map<Order, ArrayList<MenuItem>> getOrderMap() {
        return orderMap;
    }

    public ArrayList<MenuItem> getMenuItems() {
        return menuItems;
    }

    public ArrayList<Order> getOrders() {
        return orders;
    }

    public ArrayList<User> getUsers() { return users; }

    @Override
    public void importProducts() {
        menuItems.removeAll(menuItems);
        try {
            Files.lines(Paths.get("products.csv")).forEach(line -> {
                String[] words = line.split(",");
                if (words[0].equals("Title"))
                    return;
                if (menuItems.parallelStream().noneMatch(mi -> mi.getTitle().equals(words[0]))) {
                    BaseProduct p = new BaseProduct(words[0],
                            Double.parseDouble(words[1]),
                            Integer.parseInt(words[2]),
                            Integer.parseInt(words[3]),
                            Integer.parseInt(words[4]),
                            Integer.parseInt(words[5]),
                            Integer.parseInt(words[6]));
                    p.setId(++nextProductID);
                    menuItems.add(p);
                }
            });
            productTableFrame.updateTables();
            serialize();
        } catch (IOException e) {
            e.printStackTrace();
        }
        productTableFrame.updateTables();
        serialize();
    }

    @Override
    public void createMenuItem(MenuItem item) {
        assert item != null;
        int initialSize = menuItems.size();
        item.setId(++nextProductID);
        menuItems.add(item);
        int finalSize = menuItems.size();
        assert finalSize == initialSize + 1;
        productTableFrame.updateTables();
        serialize();
    }

    @Override
    public void deleteMenuItem(MenuItem item) {
        assert item != null;
        int initialSize = menuItems.size();
        menuItems.remove(item);
        int finalSize = menuItems.size();
        assert finalSize == initialSize - 1;
        productTableFrame.updateTables();
        serialize();
    }

    @Override
    public void editBaseProduct(BaseProduct item, String title, double rating, int calories, int proteins, int fats, int sodium, int price) {
        assert item != null;
        assert title != null;
        assert rating >= 0;
        assert rating <= 5;
        assert calories >= 0;
        assert price >= 0;
        assert proteins >= 0;
        assert fats >= 0;
        assert sodium >= 0;
        item.setTitle(title);
        item.setCalories(calories);
        item.setFats(fats);
        item.setPrice(price);
        item.setProteins(proteins);
        item.setRating(rating);
        item.setSodium(sodium);
        assert item.getTitle().equals(title);
        assert item.getPrice() == price;
        assert item.getSodium() == sodium;
        assert item.getCalories() == calories;
        assert item.getProteins() == proteins;
        assert item.getFats() == fats;
        assert item.getRating() == rating;
        productTableFrame.updateTables();
        serialize();
    }

    @Override
    public void editCompositeProduct(CompositeProduct item, String title) {
        assert item != null;
        assert title != null;
        item.setTitle(title);
        assert item.getTitle().equals(title);
        productTableFrame.updateTables();
        serialize();
    }

    @Override
    public void createOrder(Order order, ArrayList<MenuItem> items) {
        assert order != null;
        assert items != null;
        order.setId(++nextOrderID);
        orderMap.put(order,items);
        orders.add(order);
        StringBuilder str = new StringBuilder
                ("Order " + order.getId() + "\nordered by client " + order.getClient().getUsername() + " on " + order.getDate() + "\ncontains: ");
        items.parallelStream().forEach(it -> str.append(it.showProduct()).append("\n"));
        int orderPrice = getOrderPrice(order);
        str.append("Total price: ").append(orderPrice);
        setChanged();
        notifyObservers(str);
        productTableFrame.updateTables();
        serialize();
        File f = new File("bills/bill "+order.getId()+".txt");
        try {
            if (!f.createNewFile())
                return;
            FileWriter fw = new FileWriter(f);
            fw.write(str.toString());
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getOrderPrice(Order order) {
        assert order != null;
        return orderMap.get(order).parallelStream().mapToInt(MenuItem::getPrice).sum();
    }

    @Override
    public void generateReport(String text) {
        assert text != null;
        System.out.println(text);
    }

    public void serialize(){
        try {
            FileOutputStream f = new FileOutputStream("DeliveryService.txt");
            ObjectOutputStream o = new ObjectOutputStream(f);
            o.writeObject(this);
            f.close();
            o.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public synchronized void addObserver(Observer o) {
        super.addObserver(o);
    }

    @Override
    public void notifyObservers(Object arg) {
        (new EmployeeFrame()).update(this, arg);
    }
}
