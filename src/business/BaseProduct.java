package business;

public class BaseProduct extends MenuItem {
    private double rating;
    private int calories;
    private int proteins;
    private int fats;
    private int sodium;
    private int price;

    public BaseProduct(String title, double rating, int calories, int proteins, int fats, int sodium, int price) {
        super(title);
        this.rating = rating;
        this.calories = calories;
        this.proteins = proteins;
        this.fats = fats;
        this.sodium = sodium;
        this.price = price;
    }

    @Override
    public double getRating() {
        return rating;
    }

    @Override
    public int getCalories() {
        return calories;
    }

    @Override
    public int getProteins() {
        return proteins;
    }

    @Override
    public int getFats() {
        return fats;
    }

    @Override
    public int getSodium() {
        return sodium;
    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public String showProduct() {
        return toString();
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public void setProteins(int proteins) {
        this.proteins = proteins;
    }

    public void setFats(int fats) {
        this.fats = fats;
    }

    public void setSodium(int sodium) {
        this.sodium = sodium;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
