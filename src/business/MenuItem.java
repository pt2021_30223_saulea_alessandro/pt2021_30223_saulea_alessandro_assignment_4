package business;

import java.io.Serializable;

public abstract class MenuItem implements Serializable {
    private int id;
    private String title;

    public MenuItem(String title) { this.title = title; }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public abstract double getRating();

    public abstract int getCalories();

    public abstract int getProteins();

    public abstract int getFats();

    public abstract int getSodium();

    public abstract int getPrice();

    public abstract String showProduct();

    @Override
    public String toString() {
        return title+", price "+getPrice();
    }
}