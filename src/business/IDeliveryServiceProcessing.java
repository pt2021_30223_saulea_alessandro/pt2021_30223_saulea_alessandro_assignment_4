package business;

import java.util.ArrayList;

public interface IDeliveryServiceProcessing{

    /**
     * list.isEmpty()
     */
    void importProducts();

    /**
     * @param item
     * @pre item!=null
     * list.size()@post = list.size()@pre + 1
     */
    void createMenuItem(MenuItem item);

    /**
     * @param item
     * @pre item!=null
     * list.size()@post = list.size()@pre - 1
     */
    void deleteMenuItem(MenuItem item);

    /**
     *
     * @param item
     * @param title
     * @param rating
     * @param calories
     * @param proteins
     * @param fats
     * @param sodium
     * @param price
     * @pre item!=null
     * @pre title!=null
     * @pre rating>=0
     * @pre rating<=5
     * @pre calories>=0
     * @pre proteins>=0
     * @pre fats>=0
     * @pre sodium>=0
     * @pre price>=0
     * item@post.getTitle().equals(title)
     * item@post.getRating() == rating
     * item@post.getCalories() == calories
     * item@post.getProteins() == proteins
     * item@post.getFats() == fats
     * item@post.getSodium() == sodium
     * item@post.getPrice() == price
     */
    void editBaseProduct(BaseProduct item, String title, double rating, int calories, int proteins, int fats, int sodium, int price);

    /**
     *
     * @param item
     * @param title
     * @pre item!=null
     * @pre title!=null
     * item@post.getTitle().equals(title)
     */
    void editCompositeProduct(CompositeProduct item, String title);

    /**
     *
     * @param order
     * @param items
     * @pre order != null
     * @pre items != null
     *
     */
    void createOrder(Order order, ArrayList<MenuItem> items);

    /**
     *
     * @param order
     * @pre order!=null
     */
    int getOrderPrice(Order order);

    /**
     *
     * @param text
     * @pre text != null
     */
    void generateReport(String text);
}
