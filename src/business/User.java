package business;

import java.io.Serializable;

public class User implements Serializable {
    private String username;
    private final String password;
    private final int type; // 0 - client, 1 - admin, 2 - employee

    public User(String username, String password, int type) {
        this.username = username;
        this.password = password;
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public int getType() {
        return type;
    }
}
