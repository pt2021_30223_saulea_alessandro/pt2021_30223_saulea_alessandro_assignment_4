package business;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class Order implements Serializable {
    private int id;
    private final User client;
    private final Date date;

    public Order(User client) {
        this.client = client;
        date = new Date();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getClient() {
        return client;
    }

    public Date getDate() {
        return date;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id,client,date);
    }
}
