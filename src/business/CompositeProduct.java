package business;

import java.util.ArrayList;

public class CompositeProduct extends MenuItem {
    private final ArrayList<MenuItem> products;

    public CompositeProduct(String title, ArrayList<MenuItem> products) {
        super(title);
        this.products = products;
    }

    @Override
    public double getRating() { return products.stream().mapToDouble(MenuItem::getRating).average().getAsDouble();}

    @Override
    public int getCalories() {
        return products.parallelStream().mapToInt(MenuItem::getCalories).sum();
    }

    @Override
    public int getProteins() {
        return products.parallelStream().mapToInt(MenuItem::getProteins).sum();
    }

    @Override
    public int getFats() {
        return products.parallelStream().mapToInt(MenuItem::getFats).sum();
    }

    @Override
    public int getSodium() {
        return products.parallelStream().mapToInt(MenuItem::getSodium).sum();
    }

    @Override
    public int getPrice() {
        return products.parallelStream().mapToInt(MenuItem::getPrice).sum();
    }

    @Override
    public String showProduct() {
        StringBuilder str = new StringBuilder(super.toString()).append("(");
        products.parallelStream().forEach(p -> str.append(p).append("\n"));
        str.append(")");
        return str.toString();
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
