package presentation;

import business.MenuItem;
import business.Order;
import business.User;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.util.ArrayList;

import static app.Main.GUI;
import static app.Main.deliveryService;
import static app.Main.productTableFrame;

public class ClientFrame extends JFrame {

	public ClientFrame(User u) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setTitle("Client");
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 12, 426, 210);
		contentPane.add(scrollPane);

		DefaultListModel productModel = new DefaultListModel();
		JList productList = new JList(productModel);
		scrollPane.setViewportView(productList);
		
		JButton addToOrderBtn = new JButton("Add to order");
		addToOrderBtn.setBounds(22, 234, 134, 25);
		contentPane.add(addToOrderBtn);
		addToOrderBtn.addActionListener(l -> productModel.addAll(productTableFrame.getSelectedMenuItems()));
		
		JButton orderBtn = new JButton("Order!");
		orderBtn.setBounds(178, 234, 117, 25);
		orderBtn.addActionListener(l -> {
			if (productModel.isEmpty())
				JOptionPane.showMessageDialog(null,"You must select at least 1 product!");
			else{
				ArrayList<MenuItem> productsChosen = new ArrayList<>();
				for (int i=0;i<productModel.getSize();++i)
					productsChosen.add((MenuItem)productModel.get(i));
				deliveryService.createOrder(new Order(u),productsChosen);
				productModel.removeAllElements();
				JOptionPane.showMessageDialog(null,"Order made successfully");
			}
		});
		contentPane.add(orderBtn);
		
		JButton logoutBtn = new JButton("Log out");
		logoutBtn.setBounds(308, 234, 117, 25);
		contentPane.add(logoutBtn);
		logoutBtn.addActionListener(l ->{
			GUI.setVisible(false);
			productTableFrame.setVisible(false);
			GUI = new LoginFrame();
		});
		
		setVisible(true);
	}
}