package presentation;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JButton;

import static app.Main.GUI;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

public class EmployeeFrame extends JFrame implements Observer {

	private final JTextArea textArea;

	public EmployeeFrame() {
		setBounds(100, 100, 450, 300);
		setTitle("Employee ");
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton logoutBtn = new JButton("Log out");
		logoutBtn.setBounds(163, 236, 117, 25);
		contentPane.add(logoutBtn);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(12, 12, 426, 210);
		contentPane.add(scrollPane);
		
		textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		textArea.setEditable(false);

		logoutBtn.addActionListener(l ->{
			GUI.setVisible(false);
			GUI = new LoginFrame();
		});
		setVisible(true);
	}

	@Override
	public void update(Observable o, Object arg) {
		setVisible(true);
		textArea.append(arg.toString());
	}
}
