package presentation;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import business.MenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

public class ProductDetailsFrame extends JFrame{

	public ProductDetailsFrame(MenuItem item) {
		getContentPane().setLayout(null);

		setBounds(100, 100, 450, 300);
		setTitle(item.getTitle());
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(214, 49, 414, 179);
		contentPane.add(scrollPane);
		
		JTextArea productDetails = new JTextArea();
		productDetails.setText(item.toString());
		productDetails.setEditable(false);
		scrollPane.setViewportView(productDetails);
		
		setVisible(true);
	}
}
