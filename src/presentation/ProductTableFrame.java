package presentation;

import business.BaseProduct;
import business.CompositeProduct;
import business.MenuItem;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import static app.Main.deliveryService;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ProductTableFrame extends JFrame {

	private final DefaultTableModel baseProductModel;
	private final DefaultTableModel compositeProductModel;
	private final JTable baseProductTable;
	private final JTable compositeProductTable;
	private final JTextField titleField;

	public ProductTableFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 900);
		JPanel contentPane = new JPanel();
		setTitle("Product Table");
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane1 = new JScrollPane();
		scrollPane1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane1.setBounds(12, 22, 976, 360);
		contentPane.add(scrollPane1);
		
		baseProductModel = new DefaultTableModel();
		baseProductModel.setColumnIdentifiers(new Object[]{"ID","Title","Rating","Calories","Proteins","Fat","Sodium","Price"});
		baseProductTable = new JTable(baseProductModel);
		scrollPane1.setViewportView(baseProductTable);

		JScrollPane scrollPane2 = new JScrollPane();
		scrollPane2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane2.setBounds(12, 406, 976, 360);
		contentPane.add(scrollPane2);

		compositeProductModel = new DefaultTableModel();
		compositeProductModel.setColumnIdentifiers(new Object[]{"ID","Title","Rating","Calories","Proteins","Fat","Sodium","Price"});
		compositeProductTable = new JTable(compositeProductModel);
		scrollPane2.setViewportView(compositeProductTable);
		
		JLabel label1 = new JLabel("Base Products");
		label1.setBounds(12, 0, 109, 15);
		contentPane.add(label1);
		
		JLabel label2 = new JLabel("Menus");
		label2.setBounds(12, 385, 154, 15);
		contentPane.add(label2);
		
		JLabel label3 = new JLabel("Search:");
		label3.setBounds(12, 781, 63, 15);
		contentPane.add(label3);
		
		titleField = new JTextField();
		titleField.setBounds(78, 779, 114, 19);
		contentPane.add(titleField);
		titleField.setColumns(10);
		
		JButton showMenuButton = new JButton("Show menu content");
		showMenuButton.setBounds(231, 778, 175, 25);
		contentPane.add(showMenuButton);
		showMenuButton.addActionListener(l ->
			getSelectedMenuItems().parallelStream().forEach(ProductDetailsFrame::new));
		titleField.addActionListener(l -> updateTables());

		updateTables();
		setVisible(true);
	}
	
	public void updateTables(){
		String pattern = titleField.getText();
		baseProductModel.setRowCount(0);
		deliveryService.getMenuItems().stream().filter(it -> it instanceof BaseProduct).filter(it -> it.getTitle().contains(pattern)).forEach(
				item -> baseProductModel.addRow(new Object[]{item.getId(),item.getTitle(),String.format("%.3f",item.getRating()),item.getCalories(),
											item.getProteins(),item.getFats(),item.getSodium(),item.getPrice()}));
		compositeProductModel.setRowCount(0);
		deliveryService.getMenuItems().stream().filter(it -> it instanceof CompositeProduct).filter(it -> it.getTitle().contains(pattern)).forEach(
				item -> compositeProductModel.addRow(new Object[]{item.getId(),item.getTitle(),String.format("%.3f",item.getRating()),item.getCalories(),
											item.getProteins(),item.getFats(),item.getSodium(),item.getPrice()}));
	}

	public List<MenuItem> getSelectedMenuItems(){
		return Stream.concat(Arrays.stream(baseProductTable.getSelectedRows()).parallel()
			.mapToObj(row -> (int)baseProductModel.getValueAt(row,0))
			.map(id -> deliveryService.getMenuItems().parallelStream().filter(item -> item.getId()==id).findFirst().get()),
			Arrays.stream(compositeProductTable.getSelectedRows()).parallel()
			.mapToObj(row->(int)compositeProductModel.getValueAt(row, 0))
			.map(id -> deliveryService.getMenuItems().parallelStream().filter(item -> item.getId()==id).findFirst().get()))
			.collect(Collectors.toList());
	}

	public String[] getSelectedBaseProductRow(){
		int row = baseProductTable.getSelectedRow();
		String[] columns = new String[8];
		for (int i=0;i<8;++i)
			columns[i]=String.valueOf(baseProductModel.getValueAt(row,i));
		return columns;
	}

	public String[] getSelectedCompositeProductRow(){
		int row = compositeProductTable.getSelectedRow();
		String[] columns = new String[8];
		for (int i=0;i<8;++i)
			columns[i]=String.valueOf(compositeProductModel.getValueAt(row,i));
		return columns;
	}
}
