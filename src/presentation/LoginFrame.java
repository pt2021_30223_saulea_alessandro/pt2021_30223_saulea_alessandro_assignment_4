package presentation;

import business.User;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.util.Optional;

import static app.Main.*;

public class LoginFrame extends JFrame {

	private final JComboBox userTypeBox;

	public LoginFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 300, 300);
		setTitle("Food Delivery Management System");
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label1 = new JLabel("Username:");
		label1.setBounds(22, 71, 94, 15);
		contentPane.add(label1);
		
		JLabel label2 = new JLabel("Password:");
		label2.setBounds(22, 112, 94, 15);
		contentPane.add(label2);
		
		JTextField usernameField = new JTextField();
		usernameField.setBounds(134, 69, 114, 19);
		contentPane.add(usernameField);
		usernameField.setColumns(10);
		
		JPasswordField passwordField = new JPasswordField();
		passwordField.setBounds(134, 110, 114, 19);
		contentPane.add(passwordField);

		JButton loginButton = new JButton("Login");
		loginButton.setBounds(85, 155, 117, 25);
		loginButton.addActionListener(l -> {
			String username = usernameField.getText();
			String password = passwordField.getText();
			Optional<User> opt = deliveryService.getUsers().parallelStream().filter(user -> user.getUsername().equals(username)).findFirst();
			if (opt.isEmpty()) {
				JOptionPane.showMessageDialog(null, "User doesn't exist!");
				return;
			}
			User u = opt.get();
			if (!u.getPassword().equals(password)) {
				JOptionPane.showMessageDialog(null, "Wrong passoword!");
				return;
			}
			setVisible(false);
			if (u.getType() == 0) {
				GUI = new ClientFrame(u);
				productTableFrame = new ProductTableFrame();
			}
			else if (u.getType() == 1){
				GUI = new AdminFrame();
				productTableFrame = new ProductTableFrame();
			}
		});
		contentPane.add(loginButton);

		JButton registerButton = new JButton("Register");
		registerButton.setBounds(12, 203, 94, 25);
		registerButton.addActionListener(l -> {
			String username = usernameField.getText();
			String password = passwordField.getText();
			Optional<User> opt = deliveryService.getUsers().parallelStream().filter(user -> user.getUsername().equals(username)).findFirst();
			if (opt.isPresent())
				JOptionPane.showMessageDialog(null,"User already exists!");
			else{
				User u = new User(username,password,getUserType());
				deliveryService.getUsers().add(u);
				deliveryService.serialize();
				setVisible(false);
				if (u.getType() == 0) {
					GUI = new ClientFrame(u);
					productTableFrame = new ProductTableFrame();
				}
				else if (u.getType() == 1){
					GUI = new AdminFrame();
					productTableFrame = new ProductTableFrame();
				}
			}
		});
		contentPane.add(registerButton);
		
		JLabel label3 = new JLabel("as:");
		label3.setBounds(119, 208, 35, 15);
		contentPane.add(label3);
		
		userTypeBox = new JComboBox(new String[] {"client","administrator","employee"});
		userTypeBox.setBounds(154, 203, 134, 24);
		contentPane.add(userTypeBox);
		setVisible(true);
	}

	private int getUserType() {
		String type = (String)userTypeBox.getSelectedItem();
		assert type != null;
		if (type.equals("client"))
			return 0;
		else if (type.equals("administrator"))
			return 1;
		else
			return 2;
	}
}
