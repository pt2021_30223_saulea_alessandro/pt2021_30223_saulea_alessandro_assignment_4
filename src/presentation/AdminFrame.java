package presentation;

import business.BaseProduct;
import business.CompositeProduct;
import business.MenuItem;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import static app.Main.*;
import com.toedter.calendar.JDateChooser;

public class AdminFrame extends JFrame {

	public AdminFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 640, 480);
		setTitle("Administrator");
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton importButton = new JButton("Import");
		importButton.setBounds(55, 12, 117, 25);
		importButton.addActionListener(l -> deliveryService.importProducts());
		contentPane.add(importButton);
		
		JLabel label1 = new JLabel("Title:");
		label1.setBounds(12, 49, 48, 15);
		contentPane.add(label1);

		JTextField titleField = new JTextField();
		titleField.setBounds(88, 47, 114, 19);
		contentPane.add(titleField);
		titleField.setColumns(10);
		
		JLabel label2 = new JLabel("Rating:");
		label2.setBounds(12, 76, 62, 15);
		contentPane.add(label2);

		JTextField ratingField = new JTextField();
		ratingField.setText("");
		ratingField.setBounds(88, 74, 114, 19);
		contentPane.add(ratingField);
		ratingField.setColumns(10);
		
		JLabel label3 = new JLabel("Calories:");
		label3.setBounds(12, 103, 70, 15);
		contentPane.add(label3);

		JTextField caloriesField = new JTextField();
		caloriesField.setBounds(88, 101, 114, 19);
		contentPane.add(caloriesField);
		caloriesField.setColumns(10);
		
		JLabel label4 = new JLabel("Proteins:");
		label4.setBounds(12, 130, 70, 15);
		contentPane.add(label4);

		JTextField proteinField = new JTextField();
		proteinField.setBounds(88, 128, 114, 19);
		contentPane.add(proteinField);
		proteinField.setColumns(10);
		
		JLabel label5 = new JLabel("Fats:");
		label5.setBounds(12, 157, 70, 15);
		contentPane.add(label5);

		JTextField fatField = new JTextField();
		fatField.setText("");
		fatField.setBounds(88, 155, 114, 19);
		contentPane.add(fatField);
		fatField.setColumns(10);

		JTextField sodiumField = new JTextField();
		sodiumField.setBounds(88, 182, 114, 19);
		contentPane.add(sodiumField);
		sodiumField.setColumns(10);

		JTextField priceField = new JTextField();
		priceField.setBounds(88, 209, 114, 19);
		contentPane.add(priceField);
		priceField.setColumns(10);
		
		JLabel label6 = new JLabel("Sodium:");
		label6.setBounds(12, 184, 70, 15);
		contentPane.add(label6);
		
		JLabel label7 = new JLabel("Price:");
		label7.setBounds(12, 211, 70, 15);
		contentPane.add(label7);
		
		JButton addProductButton = new JButton("Add product");
		addProductButton.setBounds(46, 236, 126, 25);
		addProductButton.addActionListener(l -> {
			String title = titleField.getText();
			if (deliveryService.getMenuItems().stream().anyMatch(mi -> mi.getTitle().equals(title))){
				JOptionPane.showMessageDialog(null,"Product "+title+" already exists!");
				return;
			}
			try{
				double rating = Double.parseDouble(ratingField.getText());
				int calories = Integer.parseInt(caloriesField.getText()),proteins = Integer.parseInt(proteinField.getText()),
				fats = Integer.parseInt(fatField.getText()),sodium = Integer.parseInt(sodiumField.getText()),
				price = Integer.parseInt(priceField.getText());
				if (rating < 0 || rating > 5 || calories < 0 || proteins < 0 || fats < 0 || sodium < 0 || price < 0)
					JOptionPane.showMessageDialog(null,"Invalid data!");
				else
					deliveryService.createMenuItem(new BaseProduct(title,rating,calories,proteins,fats,sodium,price));
			}catch (NumberFormatException nfe){
				JOptionPane.showMessageDialog(null,"Invalid data!");
			}
		});
		contentPane.add(addProductButton);
		
		JButton editProductButton = new JButton("Edit product");
		editProductButton.setBounds(347, 12, 126, 25);
		editProductButton.addActionListener(l -> {
			if (productTableFrame.getSelectedMenuItems().size() != 1) {
				JOptionPane.showMessageDialog(null, "Select only one product!");
				return;
			}
			MenuItem item = productTableFrame.getSelectedMenuItems().get(0);
			String initialTitle = item.getTitle();
			if (item instanceof BaseProduct){
				String[] columns = productTableFrame.getSelectedBaseProductRow();
				String title = columns[1];
				if (!initialTitle.equals(title) && deliveryService.getMenuItems().stream().anyMatch(it -> it.getTitle().equals(title))){
					JOptionPane.showMessageDialog(null,"Product "+title+" already exists");
					return;
				}
				double rating = Double.parseDouble(columns[2]);
				int calories = Integer.parseInt(columns[3]), proteins = Integer.parseInt(columns[4]),
				fats = Integer.parseInt(columns[5]),sodium = Integer.parseInt(columns[6]),price = Integer.parseInt(columns[7]);
				if (rating < 0 || rating > 5 || calories < 0 || proteins < 0 || fats < 0 || sodium < 0 || price < 0)
					JOptionPane.showMessageDialog(null, "Invalid data!");
				else
					deliveryService.editBaseProduct((BaseProduct) item, title, rating, calories, proteins, fats, sodium, price);
			}
			else{
				String[] columns = productTableFrame.getSelectedCompositeProductRow();
				String title = columns[1];
				if (!initialTitle.equals(title) && deliveryService.getMenuItems().stream().anyMatch(it -> it.getTitle().equals(title))){
					JOptionPane.showMessageDialog(null,"Product "+title+" already exists");
					return;
				}
				deliveryService.editCompositeProduct((CompositeProduct)item,title);
			}
		});
		contentPane.add(editProductButton);
		
		JButton deleteProductButton = new JButton("Delete product");
		deleteProductButton.setBounds(483, 12, 145, 25);
		deleteProductButton.addActionListener(l -> productTableFrame.getSelectedMenuItems().forEach(mi -> deliveryService.deleteMenuItem(mi)));
		contentPane.add(deleteProductButton);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(214, 49, 414, 179);
		contentPane.add(scrollPane);

		DefaultListModel productModel = new DefaultListModel();
		JList productList = new JList(productModel);
		scrollPane.setViewportView(productList);

		JButton addToComposeButton = new JButton("Add to composite");
		addToComposeButton.addActionListener(l -> productModel.addAll(productTableFrame.getSelectedMenuItems()));
		addToComposeButton.setBounds(212, 236, 157, 25);
		contentPane.add(addToComposeButton);

		JButton createCompositeButton = new JButton("Create composite product");
		createCompositeButton.setBounds(395, 236, 218, 25);
		contentPane.add(createCompositeButton);
		createCompositeButton.addActionListener(l -> {
			if (productModel.isEmpty()){
				JOptionPane.showMessageDialog(null,"You must select at least 1 product!");
				return;
			}
			String title = titleField.getText();
			if (deliveryService.getMenuItems().stream().anyMatch(mi -> mi.getTitle().equals(title))){
				JOptionPane.showMessageDialog(null,"Product "+title+" already exists!");
				return;
			}
			ArrayList<MenuItem> productsChosen = new ArrayList<>();
			for (int i=0;i<productModel.getSize();++i)
				productsChosen.add((MenuItem)productModel.get(i));
			deliveryService.createMenuItem(new CompositeProduct(title, productsChosen));
			productModel.removeAllElements();
		});

		JLabel label8 = new JLabel("Products chosen:");
		label8.setBounds(212, 17, 135, 15);
		contentPane.add(label8);
		
		JButton logoutBtn = new JButton("Log out");
		logoutBtn.setBounds(337, 408, 117, 25);
		contentPane.add(logoutBtn);
		
		JLabel label9 = new JLabel("Generate reports:");
		label9.setBounds(12, 273, 145, 15);
		contentPane.add(label9);
		
		JLabel label10 = new JLabel("Orders performed between hours             and");
		label10.setBounds(12, 300, 335, 15);
		contentPane.add(label10);
		
		JSpinner startHourSpinner = new JSpinner(new SpinnerNumberModel(0,0,24,1));
		startHourSpinner.setBounds(252, 298, 48, 20);
		contentPane.add(startHourSpinner);
		
		JSpinner endHourSpinner = new JSpinner(new SpinnerNumberModel(24,0,24,1));
		endHourSpinner.setBounds(336, 298, 48, 20);
		contentPane.add(endHourSpinner);

		JLabel label11 = new JLabel("Products ordered more than             times");
		label11.setBounds(12, 327, 302, 15);
		contentPane.add(label11);

		JTextField nrTimesField = new JTextField();
		nrTimesField.setBounds(214, 325, 50, 19);
		contentPane.add(nrTimesField);
		nrTimesField.setColumns(10);

		JLabel label12 = new JLabel("Clients that have ordered more than             times whose value is higher than");
		label12.setBounds(12, 354, 555, 15);
		contentPane.add(label12);

		JTextField nrOrdersField = new JTextField();
		nrOrdersField.setBounds(273, 352, 50, 19);
		contentPane.add(nrOrdersField);
		nrOrdersField.setColumns(10);

		JTextField minPriceField = new JTextField();
		minPriceField.setBounds(565, 352, 50, 19);
		contentPane.add(minPriceField);
		minPriceField.setColumns(10);

		JLabel label13 = new JLabel("The products ordered within the day of");
		label13.setBounds(12, 381, 288, 15);
		contentPane.add(label13);

		JDateChooser dateChooser = new JDateChooser();
		dateChooser.setBounds(301, 379, 172, 19);
		contentPane.add(dateChooser);

		JButton generateBtn = new JButton("Generate report");
		generateBtn.setBounds(157, 408, 157, 25);
		contentPane.add(generateBtn);
		generateBtn.addActionListener(l -> {
			int startHour = (int)startHourSpinner.getValue();
			int endHour = (int)endHourSpinner.getValue();
			StringBuilder str = new StringBuilder();
			if (startHour>=endHour)
				JOptionPane.showMessageDialog(null,"Start hour greater than end hour!");
			else
				str.append("Orders performed between hours ").append(startHour).append(" and ").append(endHour).append(": ");

			deliveryService.getOrders().stream().filter(o -> o.getDate().getHours()>=startHour && o.getDate().getHours()<endHour)
					.forEach(o -> str.append(o.getId()).append(" "));
			try{
				int nrTimes = Integer.parseInt(nrTimesField.getText());
				str.append("\nProducts ordered more than ").append(nrTimes).append(" times:\n");
				ArrayList<MenuItem>concat = new ArrayList<>();
				deliveryService.getOrderMap().values().forEach(concat::addAll);
				concat.stream().distinct().filter(mi -> Collections.frequency(concat,mi)>nrTimes).forEach(mi -> str.append(mi).append("\n"));
			}catch (NumberFormatException nfe){
				JOptionPane.showMessageDialog(null,"Invalid input for the 2nd report!");
			}
			try{
				int nrOrders = Integer.parseInt(nrOrdersField.getText());
				int minPrice = Integer.parseInt(minPriceField.getText());
				str.append("Clients with more than ").append(nrOrders).append(" orders which cost more than ").append(minPrice).append(":\n");
				deliveryService.getUsers().stream().filter(u -> u.getType() == 0).filter(c ->
						deliveryService.getOrders().stream().filter(o -> o.getClient() == c && deliveryService.getOrderPrice(o)>minPrice)
								.count()>nrOrders).forEach(c -> str.append(c.getUsername()).append("\n"));
			}catch (NumberFormatException nfe){
				JOptionPane.showMessageDialog(null,"Invalid input for the 3rd report!");
			}
			Date date = dateChooser.getDate();
			str.append("Products ordered within ").append(date).append("\n");
			ArrayList<MenuItem>concat = new ArrayList<>();
			deliveryService.getOrderMap().keySet().stream().filter(order -> sameDay(order.getDate(),date))
					.map(order -> deliveryService.getOrderMap().get(order)).parallel().forEach(concat::addAll);
			concat.stream().distinct().forEach(mi -> str.append(mi).append(" ").append(Collections.frequency(concat,mi)).append("\n"));
			deliveryService.generateReport(str.toString());
		});
		logoutBtn.addActionListener(l ->{
			GUI.setVisible(false);
			productTableFrame.setVisible(false);
			GUI = new LoginFrame();
		});
		setVisible(true);
	}

	private boolean sameDay(Date d1, Date d2){
		return d1.getYear() == d2.getYear() && d1.getMonth() == d2.getMonth() && d1.getDate() == d2.getDate();
	}
}